# Instrucciones del entorno

Para levantar localmente el proyecto se debe generar un archivo .env donde se guardarán las variables de entorno a utilizar. 
Dejo el archivo .env.dist para utilizarse de plantilla, simplemente hacer una copia y renombrar.

# Librerias utilizadas en el proyecto

Este proyecto utilizó las siguientes librerias:

- dotenv
- express
- lodash
- nodemon
- eslint
- supertest
- should
- morgan

## Comandos del proyecto

Para poder correr el proyecto simplemente con usar `npm start`, teniendo el archivo .env creado, es suficiente.
Para el desarrollo se utilizó nodemon, para acceder utilizar el comando `npm run dev`.
Para correr los tests realizados con mocha, correrlos con `npm test`.