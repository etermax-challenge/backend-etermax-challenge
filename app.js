import express, { json, urlencoded } from 'express';
import cors from 'cors';
import morgan from 'morgan';
import * as dotenv from 'dotenv';
import routes from './lib/routes/index.js';

dotenv.config();

function initialize() {
  const app = express();
  app.use(cors());
  app.use(morgan('dev'));
  app.use(json());
  app.use(urlencoded({ extended: true }));
  Object.keys(routes).forEach((key) => {
    app.use(`/api/${key}`, routes[key]);
  });

  app.use((req, res, next) => {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
  });

  app.use((err, req, res, next) => {
    if (res.headersSent) {
      return next(err);
    }
    const error = {};
    error.status = err.status;
    if (req.app.get('env') === 'development') {
      error.message = err.message;
      error.stack = err.stack;
    }
    return res.status(err.status || 500).json({
      error,
    });
  });

  return app;
}

export default initialize;
