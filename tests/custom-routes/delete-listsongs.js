import request from 'supertest';
import 'should';
import initialize from '../../app.js';
import { ClearList, InitializeList, ReturnSavedListSongs } from '../../lib/models/listsong.js';

const { TEST_TOKEN } = process.env;

describe('DELETE LISTSONGS', () => {
  let application;

  before(() => {
    application = initialize();
  });

  beforeEach(() => InitializeList());
  afterEach(() => ClearList());

  it('listsongs/delete - Should fail without a Bearer Token', () => request(application)
    .delete('/api/listsongs/Test')
    .set('Accept', 'application/json')
    .expect(401)
    .then((response) => {
      response.body.code.should.be.equal('unauthorized');
      response.body.message.should.be.equal('Unauthorized');
    }));

  it('listsongs/delete - Should delete a list', () => request(application)
    .delete('/api/listsongs/Lista 3')
    .set('Accept', 'application/json')
    .set('Authorization', `Bearer ${TEST_TOKEN}`)
    .expect(200)
    .then((response) => {
      response.body.message.should.be.equal('List deleted successfully');
      ReturnSavedListSongs().length.should.be.equal(0);
    }));

  it('listsongs/delete - Should fail if list not exists', () => request(application)
    .delete('/api/listsongs/Lista 20')
    .set('Accept', 'application/json')
    .set('Authorization', `Bearer ${TEST_TOKEN}`)
    .expect(400)
    .then((response) => {
      response.body.error.should.be.equal('List not Found');
      ReturnSavedListSongs().length.should.be.equal(1);
    }));
});
