import request from 'supertest';
import 'should';
import initialize from '../../app.js';
import { ClearList, InitializeList, ReturnSavedListSongs } from '../../lib/models/listsong.js';

const { TEST_TOKEN } = process.env;

describe('EDIT LISTSONGS', () => {
  let application;

  before(() => {
    application = initialize();
  });

  beforeEach(() => InitializeList());
  afterEach(() => ClearList());

  it('listsongs/edit - Should fail without a Bearer Token', () => request(application)
    .put('/api/listsongs/edit')
    .set('Accept', 'application/json')
    .expect(401)
    .then((response) => {
      response.body.code.should.be.equal('unauthorized');
      response.body.message.should.be.equal('Unauthorized');
    }));

  it('listsongs/edit - Should edit a list', () => request(application)
    .put('/api/listsongs/edit')
    .set('Accept', 'application/json')
    .set('Authorization', `Bearer ${TEST_TOKEN}`)
    .send(
      {
        name: 'Lista 3',
        songs: [
          {
            title: 'cancion 1',
            artist: 'artista 1',
            album: 'album 1',
          },
          {
            title: 'cancion 2',
            artist: 'artista 7',
            album: 'album 3',
          },
        ],
      },
    )
    .expect(200)
    .then((response) => {
      response.body.message.should.be.equal('List edited successfully');
      ReturnSavedListSongs().should.containDeep([
        {
          name: 'Lista 3',
          songs: [
            {
              title: 'cancion 1',
              artist: 'artista 1',
              album: 'album 1',
            },
            {
              title: 'cancion 2',
              artist: 'artista 7',
              album: 'album 3',
            },
          ],
        },
      ]);
    }));

  it('listsongs/edit - Should fail if list not exists', () => request(application)
    .put('/api/listsongs/edit')
    .set('Accept', 'application/json')
    .set('Authorization', `Bearer ${TEST_TOKEN}`)
    .expect(400)
    .send(
      {
        name: 'Lista 4',
        songs: [
          {
            title: 'cancion 1',
            artist: 'artista 1',
            album: 'album 1',
          },
          {
            title: 'cancion 2',
            artist: 'artista 7',
            album: 'album 3',
          },
          {
            title: 'cancion 3',
            artist: 'artista 2',
            album: 'album 5',
          },
        ],
      },
    )
    .then((response) => {
      response.body.error.should.be.equal('List not Found');
    }));

  it('listsongs/edit - Should fail if body is missing', () => request(application)
    .put('/api/listsongs/edit')
    .set('Accept', 'application/json')
    .set('Authorization', `Bearer ${TEST_TOKEN}`)
    .expect(400)
    .then((response) => {
      response.body.code.should.be.equal('missing_required_fields');
      response.body.message.should.be.equal('Missing Required Fields');
    }));
});
