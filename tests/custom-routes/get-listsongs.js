import request from 'supertest';
import 'should';
import initialize from '../../app.js';
import { ClearList, InitializeList } from '../../lib/models/listsong.js';

const { TEST_TOKEN } = process.env;

describe('GET LISTSONGS BY SONG', () => {
  let application;

  before(() => {
    application = initialize();
  });

  beforeEach(() => InitializeList());
  afterEach(() => ClearList());

  it('listsongs/bySong - Should fail without a Bearer Token', () => request(application)
    .get('/api/listsongs/bySong')
    .set('Accept', 'application/json')
    .expect(401)
    .then((response) => {
      response.body.code.should.be.equal('unauthorized');
      response.body.message.should.be.equal('Unauthorized');
    }));

  it('listsongs/bySong - Should return a list', () => request(application)
    .get('/api/listsongs/bySong')
    .set('Accept', 'application/json')
    .set('Authorization', `Bearer ${TEST_TOKEN}`)
    .query(
      {
        title: 'cancion 3',
        artist: 'artista 2',
        album: 'album 5',
      },
    )
    .expect(200)
    .then((response) => {
      response.body.should.containDeep([
        {
          name: 'Lista 3',
          songs: [
            {
              title: 'cancion 1',
              artist: 'artista 1',
              album: 'album 1',
            },
            {
              title: 'cancion 2',
              artist: 'artista 7',
              album: 'album 3',
            },
            {
              title: 'cancion 3',
              artist: 'artista 2',
              album: 'album 5',
            },
          ],
        },
      ]);
    }));

  it('listsongs/bySong - Should fail if list not exists', () => request(application)
    .get('/api/listsongs/bySong')
    .set('Accept', 'application/json')
    .set('Authorization', `Bearer ${TEST_TOKEN}`)
    .query(
      {
        title: 'cancion 99',
        artist: 'artista 22',
        album: 'album 55',
      },
    )
    .expect(400)
    .then((response) => {
      response.body.error.should.be.equal('No lists were found');
    }));

  it('listsongs/bySong - Should fail if queryparams are missing', () => request(application)
    .get('/api/listsongs/bySong')
    .set('Accept', 'application/json')
    .set('Authorization', `Bearer ${TEST_TOKEN}`)
    .query(
      {
        title: 'cancion 99',
        artist: 'artista 22',
      },
    )
    .expect(400)
    .then((response) => {
      response.body.code.should.be.equal('missing_params');
      response.body.message.should.be.equal('Missing Params');
    }));
});
