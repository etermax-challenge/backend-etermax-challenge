import request from 'supertest';
import 'should';
import initialize from '../../app.js';
import { ClearList, InitializeList, ReturnSavedListSongs } from '../../lib/models/listsong.js';

const { TEST_TOKEN } = process.env;

describe('CREATE LISTSONGS', () => {
  let application;

  before(() => {
    application = initialize();
  });

  beforeEach(() => InitializeList());
  afterEach(() => ClearList());

  it('listsongs/create - Should fail without a Bearer Token', () => request(application)
    .post('/api/listsongs/create')
    .set('Accept', 'application/json')
    .expect(401)
    .then((response) => {
      response.body.code.should.be.equal('unauthorized');
      response.body.message.should.be.equal('Unauthorized');
    }));

  it('listsongs/create - Should create a new Song List', () => request(application)
    .post('/api/listsongs/create')
    .set('Accept', 'application/json')
    .set('Authorization', `Bearer ${TEST_TOKEN}`)
    .expect(200)
    .send(
      {
        name: 'Lista 4',
        songs: [
          {
            title: 'cancion 1',
            artist: 'artista 1',
            album: 'album 1',
          },
          {
            title: 'cancion 2',
            artist: 'artista 7',
            album: 'album 3',
          },
          {
            title: 'cancion 3',
            artist: 'artista 2',
            album: 'album 5',
          },
        ],
      },
    )
    .then((response) => {
      response.body.message.should.be.equal('List created successfully');
      ReturnSavedListSongs().length.should.be.equal(2);
    }));

  it('listsongs/create - Should fail without required fields', () => request(application)
    .post('/api/listsongs/create')
    .set('Accept', 'application/json')
    .set('Authorization', `Bearer ${TEST_TOKEN}`)
    .expect(400)
    .send(
      {
        name: 'Lista 4',
      },
    )
    .then((response) => {
      response.body.code.should.be.equal('missing_body');
      response.body.message.should.be.equal('Missing Body');
    }));

  it('listsongs/create - Should fail with empty songs in list', () => request(application)
    .post('/api/listsongs/create')
    .set('Accept', 'application/json')
    .set('Authorization', `Bearer ${TEST_TOKEN}`)
    .expect(400)
    .send(
      {
        name: 'Lista 4',
        songs: [],
      },
    )
    .then((response) => {
      response.body.code.should.be.equal('missing_required_fields');
      response.body.message.should.be.equal('Missing Required Fields');
    }));

  it('listsongs/create - Should fail if list exists already', () => request(application)
    .post('/api/listsongs/create')
    .set('Accept', 'application/json')
    .set('Authorization', `Bearer ${TEST_TOKEN}`)
    .expect(400)
    .send(
      {
        name: 'Lista 3',
        songs: [
          {
            title: 'cancion 1',
            artist: 'artista 1',
            album: 'album 1',
          },
          {
            title: 'cancion 2',
            artist: 'artista 7',
            album: 'album 3',
          },
          {
            title: 'cancion 3',
            artist: 'artista 2',
            album: 'album 5',
          },
        ],
      },
    )
    .then((response) => {
      response.body.error.should.be.equal('List already exists');
    }));

  it('listsongs/create - Should fail if songs fields are missing', () => request(application)
    .post('/api/listsongs/create')
    .set('Accept', 'application/json')
    .set('Authorization', `Bearer ${TEST_TOKEN}`)
    .expect(400)
    .send(
      {
        name: 'Lista 3',
        songs: [
          {
            title: 'cancion 1',
            album: 'album 1',
          },
          {
            title: 'cancion 2',
            artist: 'artista 7',
          },
          {
            artist: 'artista 2',
            album: 'album 5',
          },
        ],
      },
    )
    .then((response) => {
      response.body.code.should.be.equal('invalid_song_data');
      response.body.message.should.be.equal('Invalid Song Data');
    }));
});
