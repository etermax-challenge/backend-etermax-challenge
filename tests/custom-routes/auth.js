import request from 'supertest';
import 'should';
import initialize from '../../app.js';

describe('LOGIN', () => {
  let application;

  before(() => {
    application = initialize();
  });

  it('auth/login - Should return login token', () => request(application)
    .post('/api/auth/login')
    .set('Accept', 'application/json')
    .expect(200)
    .then((response) => {
      response.body.should.have.property('token').which.is.a.String();
    }));
});
