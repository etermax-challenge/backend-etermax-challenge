function checkifBodyExists(req, res, next) {
  if (!req.body || !req.body.songs) {
    return res.status(400).json({
      code: 'missing_body',
      message: 'Missing Body',
    });
  }
  return next();
}

function checkifDataExists(req, res, next) {
  if (!req.body.name || req.body.songs.length === 0) {
    return res.status(400).json({
      code: 'missing_required_fields',
      message: 'Missing Required Fields',
    });
  }
  return next();
}

function checkifSongsExists(req, res, next) {
  let isInvalid = false;
  req.body.songs.forEach((song) => {
    if (!song.title || !song.artist || !song.album) {
      isInvalid = true;
    }
  });
  if (isInvalid) {
    return res.status(400).json({
      code: 'invalid_song_data',
      message: 'Invalid Song Data',
    });
  }
  return next();
}

function checkifSongExists(req, res, next) {
  if (!req.query.title || !req.query.artist || !req.query.album) {
    return res.status(400).json({
      code: 'missing_params',
      message: 'Missing Params',
    });
  }
  return next();
}

export {
  checkifSongExists, checkifSongsExists, checkifDataExists, checkifBodyExists,
};
