import jwt from 'jsonwebtoken';

function getJwt(req) {
  let token;
  if (req.headers.authorization && req.headers.authorization.indexOf('Bearer') !== -1) {
    token = req.headers.authorization.replace('Bearer ', '');
  } else if (req.query && req.query.jwt) {
    token = req.query.jwt;
  }
  return token;
}

function extractJwt(req, res, next) {
  const issuers = process.env.JWT_ISSUER;
  jwt.verify(
    getJwt(req),
    process.env.JWT_SECRET,
    { issuer: issuers },
    (err, decoded) => {
      if (err) {
        return res.status(401).json({
          code: 'unauthorized',
          message: 'Unauthorized',
        });
      }
      req.decodedToken = decoded;
      return next();
    },
  );
}

export default extractJwt;
