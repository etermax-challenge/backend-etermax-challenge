import lodash from 'lodash';
import Song from './song.js';

const savedListSongs = [];
export default class ListSong {
  constructor(listSongData) {
    this.name = listSongData.name;
    this.songs = listSongData.songs;
  }
}

const initialInfo = new ListSong({
  name: 'Lista 3',
  songs: [
    new Song({
      title: 'cancion 1',
      artist: 'artista 1',
      album: 'album 1',
    }),
    new Song({
      title: 'cancion 2',
      artist: 'artista 7',
      album: 'album 3',
    }),
    new Song({
      title: 'cancion 3',
      artist: 'artista 2',
      album: 'album 5',
    }),
  ],
});

export function ReturnSavedListSongs() {
  return savedListSongs;
}

export function ClearList() {
  savedListSongs.splice(0, savedListSongs.length);
}

export function SaveListOfSongs(listToSave) {
  return new Promise((resolve, reject) => {
    const filterList = savedListSongs.filter((list) => list.name === listToSave.name);
    if (filterList.length !== 0) {
      reject({
        code: 400,
        message: 'List already exists',
      });
      return;
    }
    savedListSongs.push(listToSave);
    resolve(listToSave);
  });
}

export function DeleteListOfSongs(listName) {
  return new Promise((resolve, reject) => {
    const indexOfList = savedListSongs.findIndex((list) => list.name === listName);
    if (indexOfList === -1) {
      reject({
        code: 400,
        message: 'List not Found',
      });
      return;
    }
    savedListSongs.splice(indexOfList, 1);
    resolve();
  });
}

export function FindListBySongs(songToFind) {
  return new Promise((resolve, reject) => {
    const listsFound = [];
    savedListSongs.forEach((list) => {
      list.songs.forEach((song) => {
        if (lodash.isEqual(song, songToFind)) {
          listsFound.push(list);
        }
      });
    });
    if (listsFound.length === 0) {
      reject({
        code: 400,
        message: 'No lists were found',
      });
      return;
    }
    resolve(listsFound);
  });
}

export function EditListOfSongs(listEdited) {
  return new Promise((resolve, reject) => {
    const indexOfList = savedListSongs.findIndex((list) => list.name === listEdited.name);
    if (indexOfList === -1) {
      reject({
        code: 400,
        message: 'List not Found',
      });
      return;
    }
    savedListSongs.splice(indexOfList, 1, listEdited);
    resolve();
  });
}

export function InitializeList() {
  savedListSongs.push(initialInfo);
}
