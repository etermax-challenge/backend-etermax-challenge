export default class Song {
  constructor(songData) {
    this.title = songData.title;
    this.artist = songData.artist;
    this.album = songData.album;
  }
}

export function saveSongs(songToSave) {
  return { songToSave };
}

export function getAllSongs() {
  return {};
}
