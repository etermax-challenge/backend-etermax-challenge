import { Router } from 'express';
import jwt from 'jsonwebtoken';

const { sign } = jwt;

const router = Router();

function createToken(expiresIn, issuer) {
  const token = sign(
    { data: 'login_token' },
    process.env.JWT_SECRET,
    {
      expiresIn,
      issuer,
    },
  );
  return token;
}

/*
Endpoint for creating access token.
POST 'api/auth/login'
 */
router.post('/login', (req, res) => {
  const token = createToken('2h', process.env.JWT_ISSUER);
  return res.status(200).json({ token });
});

export default router;
