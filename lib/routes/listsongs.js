import { Router } from 'express';
import extractJwt from '../utils/extract-jwt.js';
import Song from '../models/song.js';
import ListSong, {
  EditListOfSongs, SaveListOfSongs, DeleteListOfSongs, FindListBySongs,
} from '../models/listsong.js';
import {
  checkifSongExists, checkifSongsExists, checkifDataExists, checkifBodyExists,
} from '../utils/general.js';

const router = Router();

/*
Endpoint for creating a Song List.
POST 'api/listsongs/create'
 */
router.post(
  '/create',
  extractJwt,
  checkifBodyExists,
  checkifDataExists,
  checkifSongsExists,
  (req, res) => {
    const newSongs = req.body.songs.map((song) => new Song({
      title: song.title,
      artist: song.artist,
      album: song.album,
    }));
    const newList = new ListSong({
      name: req.body.name,
      songs: newSongs,
    });
    return SaveListOfSongs(newList).then(() => res.status(200).json({
      message: 'List created successfully',
    }))
      .catch((err) => {
        console.error(`Create List Song ERROR - ${err.message}`);
        return res.status(err.code).json({ error: err.message });
      });
  },
);

/*
Endpoint for deleting a Song List.
DELETE 'api/listsongs/:name'
 */
router.delete(
  '/:name',
  extractJwt,
  (req, res) => {
    const listName = req.params.name;
    return DeleteListOfSongs(listName).then(() => res.status(200).json({
      message: 'List deleted successfully',
    }))
      .catch((err) => {
        console.error(`Delete List Song ERROR - ${err.message}`);
        return res.status(err.code).json({ error: err.message });
      });
  },
);

/*
Endpoint for finding a List based on a song.
GET 'api/listsongs/bySong'
 */
router.get(
  '/bySong',
  extractJwt,
  checkifSongExists,
  (req, res) => {
    const songToFind = new Song({
      title: req.query.title,
      artist: req.query.artist,
      album: req.query.album,
    });
    return FindListBySongs(songToFind).then((listsFound) => res.status(200).json(listsFound))
      .catch((err) => {
        console.error(`Delete List Song ERROR - ${err.message}`);
        return res.status(err.code).json({ error: err.message });
      });
  },
);

/*
Endpoint for modifying a List of songs.
PUT 'api/listsongs/edit'
 */
router.put(
  '/edit',
  extractJwt,
  checkifDataExists,
  checkifSongsExists,
  (req, res) => {
    const newSongsEdited = req.body.songs.map((song) => new Song({
      title: song.title,
      artist: song.artist,
      album: song.album,
    }));
    const editedList = new ListSong({
      name: req.body.name,
      songs: newSongsEdited,
    });

    return EditListOfSongs(editedList).then(() => res.status(200).json({
      message: 'List edited successfully',
    }))
      .catch((err) => {
        console.error(`Edit List Song ERROR - ${err.message}`);
        return res.status(err.code).json({ error: err.message });
      });
  },
);

export default router;
