import Auth from './auth.js';
import ListSongs from './listsongs.js';

export default {
  Auth,
  ListSongs,
};
