import initialize from '../app.js';

try {
  const app = initialize();

  app.listen(process.env.SERVER_PORT || 3001, () => {
    console.info(`The server is running on port: ${process.env.SERVER_PORT}`);
  });
} catch (error) {
  process.exit(1);
}
